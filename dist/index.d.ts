interface databaseResult {
    success: boolean;
    error?: Error;
}
interface user {
    username: string;
    banned: boolean;
    verified: boolean;
    admin: boolean;
    special: boolean;
    items: any[];
}
interface unsafeUser extends user {
    password: string;
}
export default interface AccountDatabase {
    addUser(username: string, password: string): Promise<databaseResult>;
    getUser(username: string): Promise<unsafeUser | false>;
    getUserSafe(username: string): Promise<user | false>;
    authUser(username: string, password: string): Promise<boolean>;
    isAdmin(user: user): boolean;
    isEnabled(user: user): boolean;
    verifyUser(username: string): Promise<databaseResult>;
}
export {};
